# Mater

## Migration warning

This project has moved to [Codeberg](https://codeberg.org/rchastain/mater).

## Overview

*Mater* is a command line tool for searching mates in chess positions.

It has been written in Pascal by Valentin Albillo. You can find the original program [here](original/mater.txt).

The program has been retouched by Roland Chastain. The command line for the retouched version is a little different and, more important, [two errors](#history) have been fixed.

## Usage

*Mater* expects at less two parameters: the position (in EPD format) and the moves number.

A third parameter can be used to change the search mode, so that only mates by consecutive checks are searched.

For example:

    mater -position "3nn3/2p2p1k/1p1pp1p1/p2B3p/r2B2N1/7N/8/7K w KQkq -" -moves 12 -check

The same example with short key names:

    mater -p "3nn3/2p2p1k/1p1pp1p1/p2B3p/r2B2N1/7N/8/7K w KQkq -" -m 12 -c

For other options, please see [here](usage.txt).

## Examples

Here are eight problems coming from *Mater* original website. These problems are solved in the program *demo.pas*.

    fpc -Mobjfpc -Sh demo.pas

### Position 1

    mater -p "b7/PP6/8/8/7K/6B1/6N1/4R1bk w - -" -m 3

![Position 1](pictures/position1.png)

    Found mate in 3: b7a8n

### Position 2

    mater -p "8/8/1p5B/4p3/1p2k1P1/1P3n2/P4PB1/K2R4 w - -" -m 3

![Position 2](pictures/position2.png)

    Found mate in 3: h6c1

### Position 3

    mater -p "2N5/8/k2K4/8/p1PB4/P7/8/8 w - -" -m 4

![Position 3](pictures/position3.png)

    Found mate in 4: d6c7

### Position 4

    mater -p "rnbK2R1/p6p/p1kNpN1r/P3B1Q1/3P1p1p/5p2/5p1b/8 w - -" -m 4

![Position 4](pictures/position4.png)

    Found mate in 4: g5g2

### Position 5

    mater -p "8/1n2P2K/3p2p1/2p3pk/6pr/4ppr1/6p1/1b6 w - -" -m 3

![Position 5](pictures/position5.png)

    Found mate in 3: h7g7

### Position 6

    mater -p "4K1R1/PP2P3/2k5/3pP3/3B4/6P1/8/8 w - -" -m 3

![Position 6](pictures/position6.png)

    Found mate in 3: b7b8r

### Position 7

    mater -p "8/2P1P1P1/3PkP2/8/4K3/8/8/8 w - -" -m 3

![Position 7](pictures/position7.png)

    Found mate in 3: e7e8b

### Position 8

    mater -p "3nn3/2p2p1k/1p1pp1p1/p2B3p/r2B2N1/7N/8/7K w - -" -m 12 -c 

![Position 8](pictures/position8.png)

    Found mate in 12: h3g5

## Compilation

*Mater* is compiled with Free Pascal.

When Free Pascal is installed, you can use the following command to build *Mater*.

    fpc -Mobjfpc -Sh -Fulibrary -Fulibrary/flre/src mater.pas

You can also use the following command.

    make

## Home

  The home page of *Mater* is [here](https://codeberg.org/rchastain/mater).

## History

### 2021.11.06

  * Fixed pawn move generation (missing pawn color in *TestRecordPawn* procedure)

### 2021.11.07

  * Fixed demo data (solution for position 4)
  
### 2021.11.20

  * Fixed conversion of square name to integer (bug introduced in the new version of the program, reported by F. Huber)
  * Fixed en passant move generation (correction provided by F. Huber)

## Interesting links

  * [Alybadix](https://alybadix.000webhostapp.com)
  * [APwin](https://alybadix.000webhostapp.com/apwin.htm)
  * [Chest](http://turbotm.de/~heiner/Chess/chest.html)
  * [Chest UCI](https://fhub.jimdofree.com)
  * [Euclide](https://github.com/svart-riddare/euclide)
  * [Fancy](https://www.wfcc.ch/software/)
  * [Jacobi](http://wismuth.com/jacobi/)
  * [Mater UCI](https://fhub.jimdofree.com)
  * [Natch](http://natch.free.fr/Natch.html)
  * [Olive GUI](https://github.com/dturevski/olive-gui)
  * [Popeye](https://github.com/thomas-maeder/popeye)
  * [Problemist](https://problemiste.pagesperso-orange.fr)
  * [Teddy](http://problemskak.dk/download-Teddy.php)
  * [WinChloe](http://winchloe.free.fr)
