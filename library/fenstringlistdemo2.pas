
{$I directives}

uses
  SysUtils, Classes, FenStringList;

const
  CFile = '../fen/41mi4.txt';
  
var
  LStr: string;
  LList: TStringList;
  
begin
  if FileExists(CFile) then
  begin
    LList := TFenStringList.Create;
    LList.LoadFromFile(CFile);
    LStr := LList.Text;
    LList.Free;
    
    if GetFen(LStr) then
      WriteLn(LStr);
  end else
    WriteLn('File not found');
end.
