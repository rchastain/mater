
{$I directives}

uses
  SysUtils, Classes, FenStringList;

var
  LList: TStringList;
  LTime: cardinal;
  
begin
  LList := TFenStringList.Create;
  
  LTime := GetTickCount64;
  
  LList.LoadFromFile('private/collections/Mate en Dos.pgn');
{
  El Arte del Mate en Dos
  Eduardo Sadier
  https://sites.google.com/site/edusadier/home
}
  LTime := GetTickCount64 - LTime;
  WriteLn('Time elapsed ', FormatDateTime('hh:nn:ss:zzz', LTime / (1000 * SECSPERDAY)));
  WriteLn('Found ', LList.Count, ' FEN strings');
  
  LList.Free;
end.
