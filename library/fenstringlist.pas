
unit FenStringList;

(* Loads all EPD/FEN strings contained in a file. *)

interface

{$I directives}

uses
  SysUtils, Classes, FLRE;

type
  TFenStringList = class(TStringList)
    procedure LoadFromFile(const FileName: string); override;
  end;

function GetFen(var AText: string): boolean;

implementation

function LoadStringFromFile(const FileName: string): string;
var
  LStream: TFileStream;
begin
  LStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  Initialize(result);
  try
    SetLength(result, LStream.Size);
    LStream.ReadBuffer(Result[1], Length(result));
  finally
    LStream.Free;
  end;
end;

function ValidFen(const AStrict: boolean = TRUE): string;
const
{
  Regular expressions for the six fields of a FEN string.
  https://kirill-kryukov.com/chess/doc/fen.html
}
  P = '[1-8BKNPQRbknpqr]+'; // Piece placement (for one rank)
  A = '[wb]';               // Active color
  C = '([KQkq]+|\-)';       // Castling availability
  E = '([a-h][36]|\-)';     // En passant target square
  H = '\d+';                // Halfmove clock
  F = '[1-9]\d*';           // Fullmove number
begin
  result := Format('(%s/%s/%s/%s/%s/%s/%s/%s)(/)?', [P, P, P, P, P, P, P, P]);
  result := Format('%s( %s %s %s)( %s %s)?', [result, A, C, E, H, F]);
  if not AStrict then
    result := Concat('.*?', result, '.*?');
end;

procedure TFenStringList.LoadFromFile(const FileName: string);
var
  LText, LExpr: string;
  LFLRE: TFLRE;
  LResult: TFLREMultiStrings;
  i: integer;
begin
  Clear;
  
  LText := LoadStringFromFile(FileName);
  
  LExpr := ValidFen;
  
  LFLRE := TFLRE.Create(LExpr, []);
  try
    Initialize(LResult);
    if LFLRE.ExtractAll(RawByteString(LText), LResult) then
      for i := 0 to Length(LResult) - 1 do
          Append(Concat(LResult[i, 1], LResult[i, 3], LResult[i, 6]));
  finally
    SetLength(LResult, 0);
    LFLRE.Free;
  end;
end;

function GetFen(var AText: string): boolean;
var
  e: TFLRE;
  c: TFLRECaptures;
  s: TFLRERawByteString;
begin
  s := ValidFen(FALSE);
  e := TFLRE.Create(s, [{rfIGNORECASE}]);
  if e.Match(AText, c) then
  begin
    result := TRUE;
    AText := Concat(
      Copy(AText, c[1].Start, c[1].Length),
      Copy(AText, c[3].Start, c[3].Length),
      Copy(AText, c[6].Start, c[6].Length)
      );
  end else
    result := FALSE;
  e.Free;
end;

end.
