
unit FenStringList;

(* Loads all EPD/FEN strings contained in a file. *)

interface

uses
  SysUtils, Classes, RegExpr;

type
  TFenStringList = class(TStringList)
    procedure LoadFromFile(const FileName: string); override;
  end;

implementation

function LoadStringFromFile(const FileName: string): string;
var
  LStream: TFileStream;
begin
  LStream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
  Initialize(result);
  try
    SetLength(result, LStream.Size);
    LStream.ReadBuffer(Result[1], Length(result));
  finally
    LStream.Free;
  end;
end;

procedure TFenStringList.LoadFromFile(const FileName: string);
const
{
  Regular expressions for the six fields of a FEN string.
  https://kirill-kryukov.com/chess/doc/fen.html
}
  P = '[1-8BKNPQRbknpqr]+'; // Piece placement (for one rank)
  A = '[wb]';               // Active color
  C = '([KQkq]+|\-)';       // Castling availability
  E = '([a-h][36]|\-)';     // En passant target square
  H = '\d+';                // Halfmove clock
  F = '[1-9]\d*';           // Fullmove number
var
  LText, LExpr: string;
  LRegExpr: TRegExpr;
begin
  Clear;
  
  LText := LoadStringFromFile(FileName);
  
  LExpr := Format('(%s/%s/%s/%s/%s/%s/%s/%s)(/)?', [P, P, P, P, P, P, P, P]);
  LExpr := Format('%s( %s %s %s)( %s %s)?', [LExpr, A, C, E, H, F]);
  
  LRegExpr := TRegExpr.Create(LExpr);
  try
    if LRegExpr.Exec(LText) then
    repeat
      Append(Concat(LRegExpr.Match[1], LRegExpr.Match[3], LRegExpr.Match[6]));
    until not LRegExpr.ExecNext;
  finally
    LRegExpr.Free;
  end;
end;

end.
