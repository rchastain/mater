
{$I directives}

uses
  SysUtils, Classes, FenStringList;

var
  LList: TStringList;
  
begin
  LList := TFenStringList.Create;
  
  LList.LoadFromFile('fen/155075mi2.fen');
  WriteLn(LList.Count = 155075);
  
  LList.LoadFromFile('private/collections/Mate en Dos.pgn');
  WriteLn(LList.Count = 155078);
  
  LList.LoadFromFile('fen/41mi4.txt');
  WriteLn(LList.Count = 41);
  
  LList.Free;
end.
