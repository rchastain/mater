
unit MaterTypes;

interface

const
  CMaxMoves = 200;
  CMaxPieces = 16;
  CBlank = 0;
  CPawn = 1;
  CKnight = 2;
  CBishop = 3;
  CRook = 4;
  CQueen = 5;
  CKing = 6;
  COut = 7;
  CWhite = 1;
  CBlack = -1;
  CNone = 0;
  CTop = 22;
  CBottom = 99;
  CCapture = -1;
  CAny = 1;
  CShortCastle = 6;
  CLongCastle = 7;
  CEnPassant = 8;
  CShortCastleValue = 50;
  CLongCastleValue = 30;

type
  TSquareSet = set of 1..120;
  TBooleanArray = array[CBlack..CWhite] of boolean;
  TSquareSetArray = array[CBlack..CWhite] of TSquareSet;

  TPosition = record
    FBoard: array[1..120] of integer;
    FActiveColor: boolean;
    FKingCastle,
    FKingRookCastle,
    FQueenRookCastle: TBooleanArray;
    FEnPassantSquare: integer;
  end;

  TMove = record
    FFrom,
    FTo,
    FClass,
    FValue: integer;
  end;

  TMoveArray = array[1..CMaxMoves] of TMove;
  TPieceArray = array[1..CMaxPieces] of integer;

  TLocations = array[CBlack..CWhite] of record
    FKingSquare: integer;
    FPieceNumber: integer;
    FPieces: TPieceArray;
  end;

const
  CPawnValue = 100;
  CKnightValue = 300;
  CBishopValue = 300;
  CRookValue = 500;
  CQueenValue = 900;
  CKingValue = 9999;

  CPieceValue: array[CBlack * CKing..CWhite * CKing] of integer = (
    CKingValue, CQueenValue, CRookValue, CBishopValue, CKnightValue, CPawnValue, 0,
    CPawnValue, CKnightValue, CBishopValue, CRookValue, CQueenValue, CKingValue
  );

  CPawnVector:   array[1..3] of integer = (10, 9, 11);
  CKnightVector: array[1..8] of integer = (-21, -19, -12, -8, 8, 12, 19, 21);
  CBishopVector: array[1..4] of integer = (-11, -9, 9, 11);
  CRookVector:   array[1..4] of integer = (-10, -1, 1, 10);
  CQueenVector:  array[1..8] of integer = (-11, -10, -9, -1, 1, 9, 10, 11);
  CKingVector:   array[1..8] of integer = (-11, -10, -9, -1, 1, 9, 10, 11);

  CPromoSquares:     TSquareSetArray = ([CBottom - 7..CBottom], [], [CTop..CTop + 7]);
  CPawnSquares:      TSquareSetArray = ([32..39], [], [82..89]);
  CEnPassantSquares: TSquareSetArray = ([72..79], [], [42..49]);

  CQueenRookSquares: array[CBlack..CWhite] of integer = (CTop, CNone, CBottom - 7);
  CKingRookSquares:  array[CBlack..CWhite] of integer = (CTop + 7, CNone, CBottom);

  CBlankPosition: TPosition = (
    FBoard: (
      cOut, cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,
      cOut, cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cBlank, cOut,
      cOut, cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,
      cOut, cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut,   cOut
    );
    FActiveColor: FALSE;
    FKingCastle: (FALSE, FALSE, FALSE);
    FKingRookCastle: (FALSE, FALSE, FALSE);
    FQueenRookCastle: (FALSE, FALSE, FALSE);
    FEnPassantSquare: CNone
  );
  
  CShape: array[CBlack * CKing..COut] of char = ('k', 'q', 'r', 'b', 'n', 'p', ' ', 'P', 'N', 'B', 'R', 'Q', 'K', '#');
  CColor: array[boolean] of string = ('White', 'Black');
  
implementation

end.
