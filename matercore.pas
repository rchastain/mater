
(*******************************************************************)
(*                                                                 *)
(*  MATER: Mate searching program - (c) Valentin Albillo 1998      *)
(*                                                                 *)
(*      This program or parts thereof can be used for any purpose  *)
(*  whatsoever as long as proper credit is given to the copyright  *)
(*  holder. Absolutely no guarantees given, no liabilities of any  *)
(*  kind accepted. Use at your own risk.  Your using this code in  *)
(*  all or in part does indicate your acceptance of these terms.   *)
(*                                                                 *)
(*******************************************************************)

unit MaterCore;

interface

type
  TSearchMode = (smAllMoves, smChecks);

const
  CSearchModeStr: array[TSearchMode] of string = ('all moves', 'checks');

function SolveMate(
  const AFen: string;
  const AMaxMovesNumber: integer;
  const ASearchMode: TSearchMode;
  var AMovesNumber: integer
): string; overload;

function SolveMate(
  const AFen: string;
  const AMaxMovesNumber: integer;
  const ASearchMode: TSearchMode = smAllMoves
): string; overload;

var
  LVerbose: boolean = FALSE;
  LLog: boolean = TRUE;
  
implementation

uses
  SysUtils, StrUtils, MaterTypes, MaterUtils;

var
  LPosition: TPosition;
  LNodes: integer;
  
procedure SearchPieces(var AData: TLocations);
var
  LSquare: integer;
begin
  FillChar(AData, SizeOf(AData), #0);
  with LPosition do
    for LSquare := CTop to CBottom do
      if Abs(FBoard[LSquare]) in [CPawn..CKing] then
        with AData[PieceColor(FBoard[LSquare])] do
        begin
          Inc(FPieceNumber);
          FPieces[FPieceNumber] := LSquare;
          if FBoard[LSquare] = PieceColor(FBoard[LSquare]) * CKing then
            FKingSquare := LSquare;
        end;
end;

function InCheck(const AColor, AKingSquare, AOpponentKingSquare: integer): boolean;
var
  i, s, b, v: integer;
begin
  //{$IFDEF DEBUG}WriteLn(ErrOutput, Format({$I %LINE%} + ' InCheck(%s,%s)', [CColor[AColor = CBlack], SquareToStr(AKingSquare)]));{$ENDIF}
  
  result := TRUE;

  if Abs(AKingSquare - AOpponentKingSquare) in [1, 9..11] then
    Exit;

  with LPosition do
  begin
    for i := 1 to 4 do
    begin
      v := CBishopVector[i];
      s := AKingSquare;
      repeat
        Inc(s, v);
        b := FBoard[s];
      until b <> CBlank;// WriteLn(ErrOutput, {$I %LINE%}, ' s = ', SquareToStr(s)); WriteLn(ErrOutput, {$I %LINE%}, ' b = ', CShape[b]);
      if (b = -1 * AColor * CBishop) or (b = -1 * AColor * CQueen) then
      begin
        //WriteLn(ErrOutput, {$I %LINE%}, ' Exit');
        Exit;
      end;
      
      v := CRookVector[i];
      s := AKingSquare;
      repeat
        Inc(s, v);
        b := FBoard[s];
      until b <> cBlank;
      if (b = -1 * AColor * CRook) or (b = -1 * AColor * CQueen) then
      begin
        //WriteLn(ErrOutput, {$I %LINE%}, ' Exit');
        Exit;
      end;
    end;

    for i := 1 to 8 do
      if FBoard[AKingSquare + CKnightVector[i]] = -1 * AColor * CKnight then
      begin
        //WriteLn(ErrOutput, {$I %LINE%}, ' Exit');
        Exit;
      end;

    for i := 2 to 3 do
      if FBoard[AKingSquare - 1 * AColor * CPawnVector[i]] = -1 * AColor * CPawn then
      begin
        //WriteLn(ErrOutput, {$I %LINE%}, ' Exit');
        Exit;
      end;
  end;
  result := FALSE;
end;

procedure GenerateMoves(
  const AColor: integer;
  const ASquare: integer;
  var AMoves: TMoveArray;
  var AMovesCount: integer;
  AKingSquare, AEnemyKingSquare: integer;
  const ALegal: boolean;
  const ASingle: boolean;
  var AFound: boolean
);
var
  s, b, i, d: integer;
  r: TPosition;
  v, c: integer;

  procedure TestRecordMove(const ABoard, AClass, AValue: integer);
  begin
    if ALegal then
    begin
      r := LPosition;
      with LPosition do
      begin
        FBoard[s] := ABoard;
        FBoard[ASquare] := cBlank;
        if AClass = -1 * CEnPassant then
          FBoard[s + CPawnVector[1] * AColor] := cBlank;
        if InCheck(AColor, AKingSquare, AEnemyKingSquare) then
        begin
          LPosition := r;
          Exit;
        end;
        if ASingle then
        begin
          AFound := TRUE;
          LPosition := r;
          Exit;
        end;
      end;
      LPosition := r;
    end;
    Inc(AMovesCount);
    with AMoves[AMovesCount] do
    begin
      FFrom := ASquare;
      FTo := s;
      FClass := AClass;
      FValue := AValue;
    end;
  end;

  procedure TestRecordPawn;
  begin
    v := CPieceValue[Abs(b)];

    if v = 0 then
      c := CAny
    else
      c := CCapture;

    if s in CPromoSquares[AColor] then
    begin
      TestRecordMove(AColor * CQueen,  CQueen  * c, v + CQueenValue);  if AFound then Exit;
      TestRecordMove(AColor * CRook,   CRook   * c, v + CRookValue);   if AFound then Exit;
      TestRecordMove(AColor * CBishop, CBishop * c, v + CBishopValue); if AFound then Exit;
      TestRecordMove(AColor * CKnight, CKnight * c, v + CKnightValue); if AFound then Exit;
    end else
    begin
      //TestRecordMove(CPawn, c, v);
      TestRecordMove(AColor * CPawn, c, v); // 06/11/2021
      if AFound then
        Exit;
    end;
  end;

  procedure TestCastling;
  var
    i: integer;
  label
    sig;
  begin
    with LPosition do
    begin
      if not FKingCastle[AColor] then
        Exit;
      AKingSquare := ASquare;
      if FKingRookCastle[AColor] then
      begin
        for i := Succ(AKingSquare) to AKingSquare + 2 do
          if FBoard[i] <> cBlank then
            goto sig;
        if InCheck(AColor, AKingSquare, AEnemyKingSquare) then
          Exit;
        for i := Succ(AKingSquare) to AKingSquare + 2 do
          if InCheck(AColor, i, AEnemyKingSquare) then
            goto sig;
        if ASingle then
        begin
          AFound := TRUE;
          Exit;
        end;
        Inc(AMovesCount);
        with AMoves[AMovesCount] do
        begin
          FFrom := ASquare;
          FTo := AKingSquare + 2;
          FClass := CShortCastle;
          FValue := CShortCastleValue;
        end;
      end;
      sig:
      if FQueenRookCastle[AColor] then
      begin
        for i := AKingSquare - 3 to Pred(AKingSquare) do
          if FBoard[i] <> cBlank then
            Exit;
        if InCheck(AColor, AKingSquare, AEnemyKingSquare) then
          Exit;
        for i := AKingSquare - 2 to Pred(AKingSquare) do
          if InCheck(AColor, i, AEnemyKingSquare) then
            Exit;
        if ASingle then
        begin
          AFound := TRUE;
          Exit;
        end;
        Inc(AMovesCount);
        with AMoves[AMovesCount] do
        begin
          FFrom := ASquare;
          FTo := AKingSquare - 2;
          FClass := CLongCastle;
          FValue := CLongCastleValue;
        end;
      end;
    end;
  end;

begin
  {$IFDEF DEBUG}
  (*
  WriteLn(ErrOutput, Format({$I %LINE%} + ' GenerateMoves(%s,%s,%s,%s,%d,%d)', [
    CColor[AColor = CBlack],
    SquareToStr(ASquare),
    SquareToStr(AKingSquare),
    SquareToStr(AEnemyKingSquare),
    Ord(ALegal),
    Ord(ASingle)
  ]));
  *)
  {$ENDIF}
  AFound := FALSE;
  Inc(LNodes);
  with LPosition do
  begin
    AMovesCount := 0;
    case Abs(FBoard[ASquare]) of
      CPawn:
        begin
          d := -1 * AColor * CPawnVector[1];
          s := ASquare + d;
          b := FBoard[s];
          if b = cBlank then
          begin
            TestRecordPawn;
            if AFound then
              Exit;
            if ASquare in CPawnSquares[AColor] then
            begin
              Inc(s, d);
              b := FBoard[s];
              if b = cBlank then
              begin
                TestRecordPawn;
                if AFound then
                  Exit;
              end;
            end;
          end;
          for i := 2 to 3 do
          begin
            s := ASquare - 1 * AColor * CPawnVector[i];
            if s = FEnPassantSquare then
            begin
              if s in CEnPassantSquares[AColor] then
              begin
                TestRecordMove({CPawn}FBoard[ASquare], -1 * CEnPassant, CPawnValue); // 20/11/2021 (thanks to Franz Huber)
                if AFound then
                  Exit;
              end;
            end else
            begin
              b := FBoard[s];
              if Abs(b) in [CPawn..CKing] then
                if b * -1 * AColor > 0 then
                begin
                  TestRecordPawn;
                  if AFound then
                    Exit;
                end;
            end;
          end;
        end;
      CKnight:
        for i := 1 to 8 do
        begin
          s := ASquare + CKnightVector[i];
          b := FBoard[s];
          if b <> 7 then
            if b * AColor <= 0 then
            begin
              v := CPieceValue[Abs(b)];
              if v = 0 then
                c := CAny
              else
                c := CCapture;
              TestRecordMove(FBoard[ASquare], c, v);
              if AFound then
                Exit;
            end;
        end;
      CBishop:
        for i := 1 to 4 do
        begin
          s := ASquare;
          repeat
            Inc(s, CBishopVector[i]);
            b := FBoard[s];
            if b <> 7 then
              if b * AColor <= 0 then
              begin
                v := CPieceValue[Abs(b)];
                if v = 0 then
                  c := CAny
                else
                  c := CCapture;
                TestRecordMove(FBoard[ASquare], c, v);
                if AFound then
                  Exit;
              end;
          until b <> cBlank;
        end;
      CRook:
        for i := 1 to 4 do
        begin
          s := ASquare;
          repeat
            Inc(s, CRookVector[i]);
            b := FBoard[s];
            if b <> 7 then
              if b * AColor <= 0 then
              begin
                v := CPieceValue[Abs(b)];
                if v = 0 then
                  c := CAny
                else
                  c := CCapture;
                TestRecordMove(FBoard[ASquare], c, v);
                if AFound then
                  Exit;
              end;
          until b <> cBlank;
        end;
      CQueen:
        for i := 1 to 8 do
        begin
          s := ASquare;
          repeat
            Inc(s, CQueenVector[i]);
            b := FBoard[s];
            if b <> 7 then
              if b * AColor <= 0 then
              begin
                v := CPieceValue[Abs(b)];
                if v = 0 then
                  c := CAny
                else
                  c := CCapture;
                TestRecordMove(FBoard[ASquare], c, v);
                if AFound then
                  Exit;
              end;
          until b <> cBlank;
        end;
      CKing:
        begin
          for i := 1 to 8 do
          begin
            s := ASquare + CKingVector[i];
            b := FBoard[s];
            AKingSquare := s;
            if b <> COut then
              if b * AColor <= 0 then
              begin
                v := CPieceValue[Abs(b)];
                if v = 0 then
                  c := CAny
                else
                  c := CCapture;
                TestRecordMove(FBoard[ASquare], c, v);
                if AFound then
                  Exit;
              end;
          end;
          TestCastling;
          if AFound then
            Exit;
        end;
    end;
  end;
end;

function AnyMoveSide(const AColor: integer; var AData: TLocations; const AKingSquare, AEnemyKingSquare: integer): boolean;
var
  i, LCount: integer;
  LMoves: TMoveArray;
  LFound: boolean;
begin
  with AData[AColor] do
  begin
    GenerateMoves(
      AColor,
      AKingSquare,
      LMoves,
      LCount,
      AKingSquare,
      AEnemyKingSquare,
      TRUE,
      TRUE,
      LFound
    );
    if LFound then
    begin
      result := TRUE;
      Exit;
    end;
    for i := 1 to FPieceNumber do
      if FPieces[i] <> AKingSquare then
      begin
        GenerateMoves(
          AColor,
          FPieces[i],
          LMoves,
          LCount,
          AKingSquare,
          AEnemyKingSquare,
          TRUE,
          TRUE,
          LFound
        );
        if LFound then
        begin
          result := TRUE;
          Exit;
        end;
      end;
  end;
  result := FALSE;
end;

procedure DoMove(const AMove: TMove; const AColor: integer; var AKingSquare: integer);
var
  LBoard: integer;
begin
  with AMove, LPosition do
  begin
    LBoard := FBoard[FFrom];
    FBoard[FFrom] := cBlank;
    FBoard[FTo] := LBoard;
    FEnPassantSquare := CNone;
    
    case Abs(LBoard) of
      CPawn:
        begin
          if Abs(FFrom - FTo) = 20 then
            FEnPassantSquare := (FFrom + FTo) div 2;
          case Abs(FClass) of
            CKnight, CBishop, CRook, CQueen:
              FBoard[FTo] := AColor * Abs(FClass);
            CEnPassant:
              FBoard[FTo + CPawnVector[1] * AColor] := cBlank;
          end;
        end;
      CKing:
        begin
          AKingSquare := FTo;
          if FKingCastle[AColor] then
          begin
            FKingCastle[AColor] := FALSE;
            FQueenRookCastle[AColor] := FALSE;
            FKingRookCastle[AColor] := FALSE;
          end;
          case FClass of
            CShortCastle:
              begin
                FBoard[Pred(FTo)] := AColor * CRook;
                FBoard[FFrom + 3] := cBlank;
              end;
            CLongCastle:
              begin
                FBoard[Succ(FTo)] := AColor * CRook;
                FBoard[FFrom - 4] := cBlank;
              end;
          end;
        end;
      CRook:
        if FFrom = CQueenRookSquares[AColor] then
          FQueenRookCastle[AColor] := FALSE
        else if FFrom = CKingRookSquares[AColor] then
          FKingRookCastle[AColor] := FALSE;
    end;
    
    if FTo = CQueenRookSquares[-1 * AColor] then
      FQueenRookCastle[-1 * AColor] := FALSE
    else if FTo = CKingRookSquares[-1 * AColor] then
      FKingRookCastle[-1 * AColor] := FALSE;
  end;
end;

function RecursiveFindMate(const AColor, ADepth, AMaxDepth: integer; const ACheckOnly: boolean; var AMove: TMove): boolean;
label
  __NEXT__,
  __MATE__;
var
  LKingSquare, LOtherKingSquare, LMoveIndex1, LMoveIndex2, LPieceIndex1, LPieceIndex2, LNotUsed: integer;
  LLoc1, LLoc2: TLocations;
  LMove: TMove;
  LArray1, LArray2: TMoveArray;
  LMoveCount1, LMoveCount2: integer;
  LPos1, LPos2: TPosition;
  LFound, LStalemate: boolean;
begin
  {$IFDEF DEBUG}(**WriteLn(ErrOutput, Format({$I %LINE%} + ' RecursiveFindMate(%s,%d,%d)', [CColor[AColor = CBlack], ADepth, AMaxDepth]));*){$ENDIF}
  SearchPieces(LLoc1); // Référence implicite à LPosition
  LKingSquare := LLoc1[AColor].FKingSquare;
  LOtherKingSquare := LLoc1[-1 * AColor].FKingSquare;
  LPos1 := LPosition;
  
  for LPieceIndex1 := 1 to LLoc1[AColor].FPieceNumber do
  begin
    GenerateMoves(AColor, LLoc1[AColor].FPieces[LPieceIndex1], LArray1, LMoveCount1, LKingSquare, LOtherKingSquare, ADepth <> AMaxDepth, FALSE, LFound);
    //{$IFDEF DEBUG}WriteLn(ErrOutput, Format({$I %LINE%} + ' LMoveCount1 = %d', [LMoveCount1]));{$ENDIF}
    for LMoveIndex1 := 1 to LMoveCount1 do
    begin
      LMove := LArray1[LMoveIndex1];
      DoMove(LMove, AColor, LKingSquare);
      if LVerbose and (ADepth = 1) then
        Write('.');
      
      if ADepth = AMaxDepth then
        if InCheck(-1 * AColor, LOtherKingSquare, LKingSquare) then
        begin
          if InCheck(AColor, LKingSquare, LOtherKingSquare) then
            goto __NEXT__;
          
          if LMove.FClass < 0 then SearchPieces(LLoc2) else LLoc2 := LLoc1;
          
          if AnyMoveSide(-1 * AColor, LLoc2, LOtherKingSquare, LKingSquare) then
            goto __NEXT__;

          goto __MATE__;
        end else
          goto __NEXT__;
      
      if ACheckOnly then
        if not InCheck(-1 * AColor, LOtherKingSquare, LKingSquare) then
          goto __NEXT__;
      
      LStalemate := TRUE;
      if LMove.FClass < 0 then SearchPieces(LLoc2) else LLoc2 := LLoc1;
      
      for LPieceIndex2 := 1 to LLoc2[-1 * AColor].FPieceNumber do
      begin
        GenerateMoves(-1 * AColor, LLoc2[-1 * AColor].FPieces[LPieceIndex2], LArray2, LMoveCount2, LOtherKingSquare, LKingSquare, TRUE, FALSE, LFound);
        //{$IFDEF DEBUG}WriteLn(ErrOutput, Format({$I %LINE%} + ' LMoveCount2 = %d', [LMoveCount2]));{$ENDIF}
        if LMoveCount2 <> 0 then
        begin
          LStalemate := FALSE;
          LPos2 := LPosition;
          for LMoveIndex2 := 1 to LMoveCount2 do
          begin
            DoMove(LArray2[LMoveIndex2], -1 * AColor, LNotUsed);
            if not RecursiveFindMate(AColor, Succ(ADepth), AMaxDepth, ACheckOnly, AMove) then
              goto __NEXT__;
            LPosition := LPos2;
          end;
        end;
      end;
      
      if ACheckOnly then
        goto __MATE__;
      
      if LStalemate then
        if InCheck(-1 * AColor, LOtherKingSquare, LKingSquare) then
          goto __MATE__
        else
          goto __NEXT__;
      
      __MATE__:
      
      if ADepth = 1 then
        AMove := LMove;
      result := TRUE;
      LPosition := LPos1;
      Exit;
      
      __NEXT__:
      
      LPosition := LPos1;
      LKingSquare := LLoc1[AColor].FKingSquare;
    end;
  end;
  
  result := FALSE;
end;

var
  LTime: cardinal;

function FindMate(const AColor: integer; const {ADepth, }AMaxDepth: integer; const ACheckOnly: boolean; var AResultDepth: integer; var AResult: TMove): boolean;
var
  LDepth: integer;
begin
  {$IFDEF DEBUG}WriteLn(ErrOutput, Format({$I %LINE%} + ' FindMate(%s,%d)', [CColor[AColor = CBlack], AMaxDepth]));{$ENDIF}
  result := FALSE;
  LDepth := {ADepth}1;
  while (LDepth <= AMaxDepth) and not result do
  begin
    if LVerbose then Write('Search depth ', LDepth, ' ');
    if RecursiveFindMate(AColor, 1, LDepth, ACheckOnly, AResult) then
    begin
      result := TRUE;
      AResultDepth := LDepth;
    end;
    Inc(LDepth);
    if LVerbose then
      WriteLn;
  end;
end;

function SolveMate(const AFen: string; const AMaxMovesNumber: integer; const ASearchMode: TSearchMode; var AMovesNumber: integer): string;
const
  CProgramName =
    '=============================================================' + LineEnding +
    ' MATER Mate searching program v1.1 (c) Valentin Albillo 1998 ' + LineEnding +
    '=============================================================';
  CSquareShape: array[boolean] of char = ('.', ':');
var
  LMoveDepth: integer;
  LActiveColor: integer;
  LMove: TMove;
  LTimeStr: string;
  i: integer;
begin
  {$IFDEF DEBUG}WriteLn(ErrOutput, Format({$I %LINE%} + ' SolveMate("%s")', [AFen]));{$ENDIF}
  result := '';
  LTime := GetTickCount64;
  LPosition := CBlankPosition;
  if LoadPosition(AFen, LPosition, LActiveColor) then
  begin
    if LLog then LogLn(Concat(
      LineEnding,
      CProgramName, LineEnding, LineEnding,
      '  ', AFen, LineEnding, 
      '  Mate in ', IntToStr(AMaxMovesNumber), ', ', CSearchModeStr[ASearchMode], LineEnding,
      PosToStr(LPosition)
    ), TRUE); // TRUE = overwrite
    
    if LVerbose then
    begin
      WriteLn;
      for i := 11 to 110 do
      begin
        if i mod 10 = 1 then
          Write('  ')
        else
          Write(' ');
        if (LPosition.FBoard[i] = CBlank) then
          Write(CSquareShape[IsDarkSquare(i)])
        else
          if (i div 10 = 1) and (i > 11) then
            Write(Chr(i mod 10 - 2 + Ord('A')))
          else if (i mod 10 = 1) and (i > 11) and (i < 101) then
            Write(Chr(10 - (i div 10) + Ord('0')))
          else
            Write(CShape[LPosition.FBoard[i]]);
        if i mod 10 = 0 then
          WriteLn;
      end;
      WriteLn;
    end;
    
    LNodes := 0;
    if FindMate(
      LActiveColor,
      //1,
      AMaxMovesNumber,
      ASearchMode = smChecks,
      LMoveDepth,
      LMove
    ) then
    begin
      result := FormatMove(LMove);
      AMovesNumber := LMoveDepth;
      if LLog then
      begin
        LTimeStr := FormatDateTime('hh:nn:ss:zzz', (GetTickCount64 - LTime) / (1000 * SECSPERDAY));
        LogLn(Format('  Found mate depth %d move %s time %s nodes %d', [LMoveDepth, result, LTimeStr, LNodes]));
      end;
    end else
      if LLog then
        LogLn('  No mate found');
  end;
end;

function SolveMate(const AFen: string; const AMaxMovesNumber: integer; const ASearchMode: TSearchMode): string;
var
  LMovesNumber: integer;
begin
  result := SolveMate(AFen, AMaxMovesNumber, ASearchMode, LMovesNumber);
end;

end.
