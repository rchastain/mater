
unit Piece;

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  BGRABitmap, BGRABitmapTypes, BitmapCollection, Constants;

type
  { TPiece }
  TPiece = class(TGraphicControl)
  private
    FIndex: integer;
    FXIni, FYIni: integer;
    FPieceType: string;
  protected
    procedure Paint; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X: integer; Y: integer); override;
  public
    constructor Create(AOwner: TComponent; ABGRACollection: TBGRABitmapCollection; AIndex: integer); reintroduce;
    destructor Destroy; override;
    property PieceType: string read FPieceType;
  end;

implementation

constructor TPiece.Create(AOwner: TComponent; ABGRACollection: TBGRABitmapCollection;
  AIndex: integer);
begin
  inherited Create(AOwner);
  FIndex := AIndex;
  Width := ABGRACollection[FIndex].Bitmap.Width;
  Height := ABGRACollection[FIndex].Bitmap.Height;
  FPieceType := ABGRACollection[FIndex].PieceType;
  Cursor := GRAB;
end;

destructor TPiece.Destroy;
begin
  inherited Destroy;
end;

procedure TPiece.Paint;
begin
  BGRABitmapCollection[FIndex].Bitmap.Draw(Canvas, 0, 0, FALSE);
end;

procedure TPiece.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: integer);
begin
  BringToFront;
  FXIni := X;
  FYIni := Y;
  Cursor := GRABBING;
  Screen.Cursor := GRABBING;
end;

procedure TPiece.MouseMove(Shift: TShiftState; X, Y: integer);
begin
  if Shift = [ssLeft] then
  begin
    Left := Left - FXIni + X;
    Top  := Top  - FYIni + Y;
  end;
end;

procedure TPiece.MouseUp(Button: TMouseButton; Shift: TShiftState; X: integer; Y: integer);
begin
  Left := 40 * Round(Left / 40) + 4;
  Top  := 40 * Round(Top  / 40) + 4;
  Cursor := GRAB;
  Screen.Cursor:= CRDEFAULT;
end;

end.

