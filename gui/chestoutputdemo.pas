
{$I directives}

uses
  ChestOutput;

const
  S = 'b7/PP6/8/8/7K/6B1/6N1/4R1bk w - - acn 1093; acs 0; bm bxa8=N; ce 32762; dm 3; pv bxa8=N Kxg2 Nb6 Bh2 a8=Q#;';

var
  bm, dm, pv: string;

begin
  if AnalyzeChestOutput(S, bm, dm, pv) then
    WriteLn(Concat('[bm] ', bm)); 
end.
