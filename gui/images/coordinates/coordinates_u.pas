
unit coordinates_u;
{$MODE objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, StdCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    BTCreate: TButton;
    procedure BTCreateClick(Sender: TObject);
    procedure FormPaint(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

uses
  BGRABitmap, BGRABitmapTypes, Types;

const
  CFileName = 'coord.png';
  
procedure TForm1.FormPaint(Sender: TObject);
var
  LImage: TBGRABitmap;
begin
  LImage := TBGRABitmap.Create(400, 400);
  if FileExists(CFileName) then
  begin
    LImage.LoadFromFile(CFileName);
    LImage.Draw(Canvas, 8, 8, FALSE);
  end;
  LImage.Free;
end;

procedure TForm1.BTCreateClick(Sender: TObject);
const
  CLetters = 'ABCDEFGH';
  CDigits = '12345678';
  //CFontName = 'Upheaval';
  //CFontName = 'Alagard';
  //CFontName = 'Free Pixel';
  //CFontName = 'Windows Command Prompt';
  //CFontName = 'GothicPixels';
  //CFontName = 'Modern DOS 8x16';
  CFontName = 'Code';
  CFontHeight = 16;
  //CFontStyle = [fsBold];
  CFontStyle = [];
var
  LBoard, LSquare, LSymbol: TBGRABitmap;
  LSize: TSize;
  i: integer;
  LPath: string;
begin
  LPath := LowerCase(CFontName);
  LPath := StringReplace(LPath, ' ', '-', [rfReplaceAll]);
  if not DirectoryExists(LPath) then
    if not CreateDir(LPath) then
      ShowMessage('Cannot create directory');
  LPath := LPath + DirectorySeparator;

  LBoard := TBGRABitmap.Create(400, 400);

  for i := 1 to 8 do
  begin
    LSquare := TBGRABitmap.Create(40, 40);
    LSquare.FontName := CFontName;
    LSquare.FontAntialias := FALSE;
    LSquare.FontHeight := CFontHeight;
    LSquare.FontStyle := CFontStyle;
    LSize := LSquare.TextSize(CLetters[i]);
    LSymbol := TBGRABitmap.Create(40, 40);
    LSquare.CopyPropertiesTo(LSymbol);
    LSymbol.TextOut((40 - LSize.cx) div 2, (40 - LSize.cy) div 2, CLetters[i], BGRABlack);
    LSquare.PutImage(0, 0, LSymbol, dmSet);
    LSquare.SaveToFile(Concat(LPath, CLetters[i], '.png'));
    LBoard.PutImage(40 * i,   0, LSymbol, dmSet);
    LBoard.PutImage(40 * i, 360, LSymbol, dmSet);
    LSquare.Free;
    LSymbol.Free;
  end;

  for i := 1 to 8 do
  begin
    LSquare := TBGRABitmap.Create(40, 40);
    LSquare.FontName := CFontName;
    LSquare.FontAntialias := FALSE;
    LSquare.FontHeight := CFontHeight;
    LSquare.FontStyle := CFontStyle;
    LSize := LSquare.TextSize(CDigits[i]);
    LSymbol := TBGRABitmap.Create(40, 40);
    LSquare.CopyPropertiesTo(LSymbol);
    LSymbol.TextOut((40 - LSize.cx) div 2, (40 - LSize.cy) div 2, CDigits[i], BGRABlack);
    LSquare.PutImage(0, 0, LSymbol, dmSet);
    LSquare.SaveToFile(Concat(LPath, CDigits[i], '.png'));
    LBoard.PutImage(  0, 40 * (9 - i), LSymbol, dmSet);
    LBoard.PutImage(360, 40 * (9 - i), LSymbol, dmSet);
    LSquare.Free;
    LSymbol.Free;
  end;

  LBoard.SaveToFile(Concat(LPath, CFileName));
  LBoard.SaveToFile(CFileName);
  LBoard.Free;

  Invalidate;
end;

end.

