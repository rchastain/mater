
program convert;
{$APPTYPE CONSOLE}

uses
  Classes, SysUtils, FileUtil, BGRABitmap, BGRABitmapTypes;

var
  files: TStringList;
  file1: string;
  img: TBGRABitmap;

begin
  files := FindAllFiles('', '*.bmp', FALSE);

  for file1 in files do
  begin
    img := TBGRABitmap.Create(file1);
    img.ReplaceColor(CSSMidnightBlue, BGRAPixelTransparent);
    img.SaveToFile(ChangeFileExt(file1, '.png'));
    img.Free;
  end;

  files.Free;
end.

