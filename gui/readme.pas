  'Mater GUI %s' + LineEnding +
  '' + LineEnding +
  'Graphical user interface for Valentin Albillo''s mate searching program.' + LineEnding +
  '' + LineEnding +
  'Uses Chest by Heiner Marxen as auxiliary search engine.' + LineEnding +
  '' + LineEnding +
  'For Linux, uses Chest 3.19 (1). For Windows, uses WinChest by Heiner Marxen and Franz Huber (2).' + LineEnding +
  '' + LineEnding +
  '' + LineEnding +
  '(1) http://turbotm.de/~heiner/Chess/chest.html' + LineEnding +
  '(2) https://fhub.jimdofree.com'
