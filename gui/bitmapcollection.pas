
unit BitmapCollection;

interface

uses
  Classes, SysUtils, Controls, Graphics, Forms, Contnrs, FileUtil, BGRABitmap;

type
  TBGRABitmapItem = class(TObject)
  private
    FBitmap: TBGRABitmap;
    FPieceType: string;
  public
    constructor Create(APieceType: string);
    destructor Destroy; override;
    property Bitmap: TBGRABitmap read FBitmap write FBitmap;
    property PieceType: string read FPieceType;
  end;

  TBGRABitmapCollection = class(TObjectList)
  private
    function GetItem(Index: integer): TBGRABitmapItem;
  public
    property Item[AIndex: integer]: TBGRABitmapItem read GetItem; default;
    function NewBGRABitmap(AFileName: string): integer;
  end;

var
  BGRABitmapCollection: TBGRABitmapCollection;

implementation

const
  CPicturesPath = 'gui/images/fritz/';

constructor TBGRABitmapItem.Create(APieceType: string);
begin
  inherited Create;
  FBitmap := TBGRABitmap.Create(Concat(CPicturesPath, APieceType, '.png'));
  FPieceType := APieceType;
end;

destructor TBGRABitmapItem.Destroy;
begin
  FreeAndNil(FBitmap);
  inherited Destroy;
end;

function TBGRABitmapCollection.GetItem(Index: integer): TBGRABitmapItem;
begin
  result := TBGRABitmapItem(inherited GetItem(Index));
end;

function TBGRABitmapCollection.NewBGRABitmap(AFileName: string): integer;
begin
  result := inherited Add(TBGRABitmapItem.Create(AFileName));
end;

procedure LoadImages;
begin
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wp');
  BGRABitmapCollection.NewBGRABitmap('wr');
  BGRABitmapCollection.NewBGRABitmap('wr');
  BGRABitmapCollection.NewBGRABitmap('wn');
  BGRABitmapCollection.NewBGRABitmap('wn');
  BGRABitmapCollection.NewBGRABitmap('wb');
  BGRABitmapCollection.NewBGRABitmap('wb');
  BGRABitmapCollection.NewBGRABitmap('wq');
  BGRABitmapCollection.NewBGRABitmap('wk');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('bp');
  BGRABitmapCollection.NewBGRABitmap('br');
  BGRABitmapCollection.NewBGRABitmap('br');
  BGRABitmapCollection.NewBGRABitmap('bn');
  BGRABitmapCollection.NewBGRABitmap('bn');
  BGRABitmapCollection.NewBGRABitmap('bb');
  BGRABitmapCollection.NewBGRABitmap('bb');
  BGRABitmapCollection.NewBGRABitmap('bq');
  BGRABitmapCollection.NewBGRABitmap('bk');
end;

initialization
  BGRABitmapCollection := TBGRABitmapCollection.Create;
  LoadImages;

finalization
  BGRABitmapCollection.Free;

end.

