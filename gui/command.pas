
unit Command;

interface

procedure ExecuteCommand(const AInterpreter, ASwitch, ACommand: string; out AResult: string);

implementation

uses
  Classes, SysUtils, Process;

procedure ExecuteCommand(const AInterpreter, ASwitch, ACommand: string; out AResult: string);
const
  CBufferSize = 2048;
var
  LProcess: TProcess;
  LStream: TStream;
  LRead: longint;
  LBuffer: array[1..CBufferSize] of byte;
begin
  Initialize(LBuffer);
  LProcess := TProcess.Create(nil);
  LProcess.Executable := AInterpreter;
  LProcess.Parameters.Add(ASwitch);
  LProcess.Parameters.Add(ACommand);
  LProcess.Options := [poUsePipes, poNoConsole];
  LProcess.Execute;
  LStream := TMemoryStream.Create;
  repeat
    LRead := LProcess.Output.Read(LBuffer, CBufferSize);
    LStream.Write(LBuffer, LRead);
  until LRead = 0;
  LProcess.Free;
  (*
  with TFileStream.Create('output.txt', fmCreate) do
  begin
    LStream.Position := 0;
    CopyFrom(LStream, LStream.Size);
    Free;
  end;
  *)
  with TStringList.Create do 
  begin
    LStream.Position := 0;
    LoadFromStream(LStream);
    AResult := Text;
    Free;
  end;
  LStream.Free;
end;

end.

