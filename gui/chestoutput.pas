
unit ChestOutput;

interface

{$I directives}

function AnalyzeChestOutput(const AChestOutput: string; out ABestMove, ADistanceToMate, APrincipalVariation: string): boolean;

implementation

uses
  SysUtils, Classes, FLRE;

function AnalyzeChestOutput(const AChestOutput: string; out ABestMove, ADistanceToMate, APrincipalVariation: string): boolean;
var
  e: TFLRE;
  c: TFLRECaptures;
begin
  result := FALSE;
  e := TFLRE.Create('.+; bm ([^;]+);.+; dm ([^;]+); pv ([^;]+);', []);

  if e.Match(AChestOutput, c) then
  begin
    result := TRUE;
    ABestMove := Copy(AChestOutput, c[1].Start, c[1].Length);
    ADistanceToMate := Copy(AChestOutput, c[2].Start, c[2].Length);
    APrincipalVariation := Copy(AChestOutput, c[3].Start, c[3].Length);
  end;
  e.Free;
end;

end.

