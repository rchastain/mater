unit aboutform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    BTClose: TButton;
    MMReadMe: TMemo;
    Panel1: TPanel;
    procedure BTCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.lfm}

resourcestring
  ReadMeText = {$I readme};
  BTCloseCaption = 'Close';
  FormCaption = 'About Mater GUI';

{ TFormAbout }

procedure TFormAbout.BTCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFormAbout.FormCreate(Sender: TObject);
begin
  MMReadMe.Text := Format(ReadMeText, [{$I version}]);
  BTClose.Caption := BTCloseCaption;
  Caption := FormCaption;
end;

end.

