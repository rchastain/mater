
unit Settings;

interface

uses
  SysUtils;

procedure LoadSettings(
  out APos: string;
  out AFenFile: TFileName;
  out APositionIndex: integer;
  out ADepth: integer
);
procedure SaveSettings(
  const APos: string;
  const AFenFile: TFileName;
  const APositionIndex: integer;
  const ADepth: integer
);
  
implementation

uses
  IniFiles;

const
  CSection = 'main';
  CDefaultPosition = 'b7/PP6/8/8/7K/6B1/6N1/4R1bk w - -';
  CDefaultDepth = 3;
  CDefaultFile = 'fen/155075mi2.fen';
{
  El Arte del Mate en Dos
  Eduardo Sadier
  https://sites.google.com/site/edusadier/home
}

var
  LIniFileName: TFileName;
  
procedure LoadSettings(
  out APos: string;
  out AFenFile: TFileName;
  out APositionIndex: integer;
  out ADepth: integer
);
begin
  with TIniFile.Create(LIniFileName) do
  try
    APos := ReadString(CSection, 'currentposition', CDefaultPosition);
    AFenFile := ReadString(CSection, 'file', CDefaultFile);
    APositionIndex := ReadInteger(CSection, 'positionindex', 1);
    ADepth := ReadInteger(CSection, 'searchdepth', CDefaultDepth);
  finally
    Free;
  end;
end;

procedure SaveSettings(
  const APos: string;
  const AFenFile: TFileName;
  const APositionIndex: integer;
  const ADepth: integer
);
begin
  with TIniFile.Create(LIniFileName) do
  try
    WriteString(CSection, 'currentposition', APos);
    WriteString(CSection, 'file', AFenFile);
    WriteInteger(CSection, 'positionindex', APositionIndex);
    WriteInteger(CSection, 'searchdepth', ADepth);
    UpdateFile;
  finally
    Free;
  end;
end;

begin
  LIniFileName := ChangeFileExt(ParamStr(0), '.ini');
end.

