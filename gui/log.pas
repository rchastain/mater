
unit Log;

interface

procedure LogLn(const ALine: string; const ARewrite: boolean = FALSE);

implementation

uses
  SysUtils;

var
  LFilename: TFilename;
  
procedure LogLn(const ALine: string; const ARewrite: boolean);
var
  LFile: TextFile;
begin
  Assign(LFile, LFilename);
  if ARewrite or not FileExists(LFileName) then
    Rewrite(LFile)
  else
    Append(LFile);
  WriteLn(LFile, DateTimeToStr(Now), ' ', ALine);
  Close(LFile);
end;

initialization
  LFilename := ChangeFileExt(ParamStr(0), '.log');
finalization
  SetLength(LFilename, 0);
end.

