
Mater GUI %s

Graphical user interface for Valentin Albillo's mate searching program.

Uses Chest by Heiner Marxen as auxiliary search engine.

For Linux, uses Chest 3.19 (1). For Windows, uses WinChest by Heiner Marxen and Franz Huber (2).

(1) http://turbotm.de/~heiner/Chess/chest.html
(2) https://fhub.jimdofree.com
