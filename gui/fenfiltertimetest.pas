
uses
  SysUtils, Classes, FenFilter;

var
  LStringFilter: TFenFilter;

const
  CDefaultFile = '../fen/155075mi2.fen';
{
  El Arte del Mate en Dos
  Eduardo Sadier
  https://sites.google.com/site/edusadier/home
}

var
  LLst: TStringList;
  LIdx, LCnt: integer;
  LTime: cardinal;
  LFile: string;
  
begin
  LFile := CDefaultFile;
  
  if (ParamCount > 0) and FileExists(ParamStr(1)) then LFile := ParamStr(1);
  
  LStringFilter := TFenFilter.Create;
  
  LLst := TStringList.Create;
  LLst.LoadFromFile(LFile);
  
  LCnt := 0;
  LTime := GetTickCount64;
  for LIdx := 0 to Pred(LLst.Count) do
    if LStringFilter.IsFen(LLst[LIdx]) then
      Inc(LCnt);
  
  LTime := GetTickCount64 - LTime;
  WriteLn('Time elapsed ', FormatDateTime('hh:nn:ss:zzz', LTime / (1000 * SECSPERDAY)));
  WriteLn('Validated ', LCnt, '/', LLst.Count);
  
  LLst.Free;
  
  LStringFilter.Free;
end.
