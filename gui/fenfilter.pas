
unit FenFilter;

interface

uses
  RegExpr;

type
  TFenFilter = class
    private
      function ExpandEmptySquares(AExpr: TRegExpr): string;
    public
      function IsFen(const AStr: string; const AAcceptEpd: boolean = TRUE): boolean;
  end;

implementation

uses
  Classes, SysUtils;

const
  CPieces    = '^[1-8BKNPQRbknpqr]+$';
  CActive    = '^[wb]$';
  CCastling  = '^[KQkq]+$|^[A-Ha-h]+$|^\-$';
  CEnPassant = '^[a-h][36]$|^\-$';
  CHalfMove  = '^\d+$';
  CFullMove  = '^[1-9]\d*$';

var
  e, f, g, h, j, k, l: TRegExpr;

function TFenFilter.ExpandEmptySquares(AExpr: TRegExpr): string;
begin
  result := '';
  with AExpr do
    result := StringOfChar('-', StrToInt(Match[0]));
end;

function TFenFilter.IsFen(const AStr: string; const AAcceptEpd: boolean): boolean;
var
  a, b: TStringList;
  i: integer;
  s: string;
begin
  a := TStringList.Create;
  b := TStringList.Create;
  (*
  ExtractStrings([' '], [], PChar(AStr), a);
  ExtractStrings(['/'], [], PChar(a[0]), b);
  *)
  SplitRegExpr(' ', AStr, a);
  
  result := (a.Count = 6) or AAcceptEpd and (a.Count = 4);

  if result then
  begin
    SplitRegExpr('/', a[0], b);
    result := (b.Count = 8);
  end;
  
  if result then
  begin
    for i := 0 to 7 do
    begin
      result := result and {ExecRegExpr(CPieces, b[i])} f.Exec(b[i]);
      if result then
      begin
        s := b[i];
        repeat
          s := e.Replace(s, @ExpandEmptySquares);
        until not {ExecRegExpr('\d', s)}e.Exec(s);
        result := result and (Length(s) = 8);
      end;
    end;
    
    result := result and {ExecRegExpr(CActive,    a[1])}g.Exec(a[1]);
    result := result and {ExecRegExpr(CCastling,  a[2])}h.Exec(a[2]);
    result := result and {ExecRegExpr(CEnPassant, a[3])}j.Exec(a[3]);
    result := result and (AAcceptEpd and (a.Count = 4) or {ExecRegExpr(CHalfMove,  a[4])}k.Exec(a[4]));
    result := result and (AAcceptEpd and (a.Count = 4) or {ExecRegExpr(CFullMove,  a[5])}l.Exec(a[5]));
  end;

  a.Free;
  b.Free;
end;

initialization
  e := TRegExpr.Create('\d');
  f := TRegExpr.Create(CPieces);
  g := TRegExpr.Create(CActive);
  h := TRegExpr.Create(CCastling);
  j := TRegExpr.Create(CEnPassant);
  k := TRegExpr.Create(CHalfMove);
  l := TRegExpr.Create(CFullMove);
finalization
  e.Free;
  f.Free;
  g.Free;
  h.Free;
  j.Free;
  k.Free;
  l.Free;
end.

