unit mainform;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus, ExtCtrls,
  StdCtrls, Spin, LResources, ClipBrd, DefaultTranslator, LazLogger;

type

  { TForm1 }

  TForm1 = class(TForm)
    BTClipboard: TButton;
    BTNext: TButton;
    BTOpen: TButton;
    BTMater: TButton;
    BTQuit: TButton;
    BTChest: TButton;
    CBCheck: TCheckBox;
    EDFile: TEdit;
    EDCount: TEdit;
    EDIndex: TEdit;
    EDChestResult: TEdit;
    EDMaterResult: TEdit;
    EDFen: TEdit;
    GBFile: TGroupBox;
    GBCount: TGroupBox;
    GBIndex: TGroupBox;
    GBParams: TGroupBox;
    GBMater: TGroupBox;
    GBFen: TGroupBox;
    GBChest: TGroupBox;
    MM: TMainMenu;
    MIApp: TMenuItem;
    MIQuit: TMenuItem;
    MIHelp: TMenuItem;
    MIAbout: TMenuItem;
    OD: TOpenDialog;
    Panel1: TPanel;
    Panel2: TPanel;
    EDDepth: TSpinEdit;
    procedure BTClipboardClick(Sender: TObject);
    procedure BTNextClick(Sender: TObject);
    procedure BTOpenClick(Sender: TObject);
    procedure BTQuitClick(Sender: TObject);
    procedure BTChestClick(Sender: TObject);
    procedure BTMaterClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure MIAboutClick(Sender: TObject);
    procedure Panel1Paint(Sender: TObject);
  private
    FPosition: string;
    FFile: TFileName;
    FPositionList: TStringList;
    FPositionIndex: integer;
    procedure SetPosition(const AEpd: string);
    procedure SetPositionFromFile(var APositionIndex: integer);
    procedure LoadPositions;
    procedure SetCaptions;
    procedure AbortProcess(const AExeName: string);
  public

  end;

var
  Form1: TForm1;

implementation

uses
  Process, StrUtils, BGRABitmap, BGRABitmapTypes, Settings, Constants, BitmapCollection,
  Piece, FenFilter, FenStringList, Command, ChestOutput, Log, AboutForm;

{$R *.lfm}

resourcestring
  BTClipboardCaption = 'Load from clipboard';
  BTClipboardHint = 'Load position from clipboard';
  BTNextCaption = 'Load from file';
  BTNextHint = 'Load position from file';
  //BTOpenCaption = '...';
  BTOpenHint = 'Select another file';
  GBFileCaption = 'Current file';
  //GBFileHint = '';
  GBCountCaption = 'Positions';
  GBCountHint = 'Number of positions in file';
  GBIndexCaption = 'Position index';
  GBIndexHint = 'Current position index';
  GBParamsCaption = 'Search parameters';
  GBParamsHint = 'Depth and search mode (all moves, only checks)';
  CBCheckCaption = 'Check only';
  CBCheckHint = 'Only mates by consecutive checks';
  GBMaterCaption = 'Mater';
  //GBMaterHint = '';
  GBChestCaption = 'Chest';
  //GBChestHint = '';
  BTStartMaterCaption = 'Start Mater';
  //BTStartMaterHint = '';
  BTStopMaterCaption = 'Stop Mater';
  //BTStopMaterHint = '';
  BTStartChestCaption = 'Start Chest';
  //BTStartChestHint = '';
  BTStopChestCaption = 'Stop Chest';
  //BTStopChestHint = '';
  BTQuitCaption = 'Quit';
  //BTQuitHint = '';
  GBFenCaption = 'EPD/FEN';
  GBFenHint = 'The current position as an EPD/FEN string';
  MIAppCaption = 'Mater GUI';
  //MIAppHint = '';
  MIHelpCaption = 'Help';
  //MIHelpHint = '';
  MIAboutCaption = 'About';
  //MIAboutHint = '';
  DoesNotContainFen = 'No EPD/FEN string in the clipboard.';

const
  CSquaresBitmap = 'gui/images/fritz/board.bmp';
  //CCoordinatesBitmap = 'gui/images/coordinates/alagard/coord.png';
  //CCoordinatesBitmap = 'gui/images/coordinates/modern-dos-8x16/coord.png';
  CCoordinatesBitmap = 'gui/images/coordinates/code/coord.png';
  //CCoordinatesBitmap = 'gui/images/coordinates/free-pixel/coord.png';
  //CCoordinatesBitmap = 'gui/images/coordinates/gothicpixels/coord.png';
{$IFDEF MSWINDOWS}
  CInterpreter = 'c:\windows\system32\cmd.exe';
  CSwitch = '/c';
  CMater = 'mater.exe';
  CChest = 'winchest.exe';
{$ELSE}
{$IFDEF LINUX}
  CInterpreter = '/bin/bash';
  CSwitch = '-c';
  CMater = './mater';
  CChest = './dchest';
{$ELSE}
{$ENDIF}
{$ENDIF}

function FenToEpd(const AFen: string): string;
var
  LPos: integer;
begin
  if WordCount(AFen, [' ']) = 6 then
  begin
    ExtractWordPos(5, AFen, [' '], LPos);
    result := Copy(AFen, 1, LPos - 2);
  end else
    result := AFen;
end;

const
  CTagSearch = 0;
  CTagStop = 1;
  CTagStopRequested = 2;
var
  LChessboard: TBGRABitmap;
  LPieces: array[0..31] of TPiece;
  LFilter: TFenFilter;

type
  TMaterThread = class(TThread)
  private
    FResult: string;
    procedure OnResult;
  protected
    procedure Execute; override;
  end;

  TChestThread = class(TThread)
  private
    FResult: string;
    procedure OnResult;
  protected
    procedure Execute; override;
  end;
  
procedure TMaterThread.Execute;
const
  CSearchMode: array[boolean] of string = ('', ' -c');
var
  LCommand, LResult: string;
begin
  with Form1 do
    LCommand := Format('%s -p "%s" -m %d%s -q', [
      CMater,
      FPosition,
      EDDepth.Value,
      CSearchMode[CBCheck.Checked]
    ]);
  Initialize(LResult);
  ExecuteCommand(CInterpreter, CSwitch, LCommand, LResult);
  LResult := Trim(LResult);
  FResult := LResult;
  Synchronize(@OnResult);
end;

procedure TMaterThread.OnResult;
begin
  with Form1 do
  begin
    EDMaterResult.Text := FResult;
    BTMater.Enabled    := TRUE;
    BTMater.Caption    := BTStartMaterCaption;
    BTMater.Tag        := CTagSearch;
  end;
end;

procedure TChestThread.Execute;
const
{$IFDEF MSWINDOWS}
  CChestCommand = 'echo %s | %s -b -Z %d%s';
  CSearchMode: array[boolean] of string = ('', ' -C 1');
{$ELSE}
{$IFDEF LINUX}
  CChestCommand = 'printf "%%s\n" "%s" | %s -b -Z %d%s';
  CSearchMode: array[boolean] of string = ('', '');
{$ELSE}
{$ENDIF}
{$ENDIF}
var
  LCommand, LResult: string;
begin
  with Form1 do
    LCommand := Format(CChestCommand, [
      FenToEpd(FPosition),
      CChest,
      EDDepth.Value,
      CSearchMode[CBCheck.Checked]
    ]);
  DebugLn(Concat('[Chest] Command ', LCommand));
  Initialize(LResult);
  ExecuteCommand(CInterpreter, CSwitch, LCommand, LResult);
  LResult := Trim(LResult);
  if Length(LResult) > 0 then DebugLn(Concat('[Chest] Result ', LResult)) else DebugLn('[Chest] No result');
  FResult := LResult;
  Synchronize(@OnResult);
end;

procedure TChestThread.OnResult;
var
  LBestMove, LDistanceToMate, LPrincipalVariation: string;
begin
  with Form1 do begin
    if Length(FResult) > 0 then begin
      if AnalyzeChestOutput(FResult, LBestMove, LDistanceToMate, LPrincipalVariation) then
      begin
        (* Une solution a été trouvée. *)
        EDChestResult.Text := Format('Found mate in %s: %s', [LDistanceToMate, LBestMove]);
        DebugLn(Concat('[Chest] Best move ', LBestMove));
        DebugLn(Concat('[Chest] Distance to mate ', LDistanceToMate));
        DebugLn(Concat('[Chest] Principal variation ', LPrincipalVariation));
      end else begin
        (* L'analyse de la réponse a échoué. *)
        EDChestResult.Clear;
        DebugLn('[Chest] Failed to analyze output');
      end;
    end else
      if Form1.BTChest.Tag = CTagStopRequested then
      begin
        (* Absence de réponse suite à une interruption du processus. *)
        EDChestResult.Clear;
        DebugLn('[Chest] Process aborted by user');
      end else begin
        (* Chest a terminé sa recherche sans trouver de solution. *)
        EDChestResult.Text := 'No mate found';
        DebugLn('[Chest] No solution found');
      end;
    BTChest.Caption := BTStartChestCaption;
    BTChest.Tag := CTagSearch;
  end;
end;

var
  LMaterThread, LChestThread: TThread;

{ TForm1 }

procedure TForm1.BTQuitClick(Sender: TObject);
begin
  Close;
end;

procedure TForm1.BTClipboardClick(Sender: TObject);
var
  LStr: string;
begin
  LStr := Trim(Clipboard.AsText);
  if GetFen(LStr) then
  begin
    SetPosition(LStr);
    EDIndex.Clear;
  end else
    ShowMessage(DoesNotContainFen);
end;

procedure TForm1.BTNextClick(Sender: TObject);
begin
  SetPositionFromFile(FPositionIndex);
end;

procedure TForm1.BTOpenClick(Sender: TObject);
begin
  if OD.Execute then
  begin
    FFile := ExtractRelativePath(ParamStr(0), OD.FileName);
    FPositionIndex := 1;
    LoadPositions;
    SetPositionFromFile(FPositionIndex);
  end;
end;

procedure TForm1.BTMaterClick(Sender: TObject);
begin
  if BTMater.Tag = CTagSearch then
  begin
    EDMaterResult.Text := '...';
    LMaterThread := TMaterThread.Create(TRUE);
    LMaterThread.Priority := tpHigher;
    LMaterThread.FreeOnTerminate := TRUE;
    LMaterThread.Start;
    BTMater.Caption := BTStopMaterCaption;
    BTMater.Tag := CTagStop;
  end else
    AbortProcess(CMater);
end;

procedure TForm1.BTChestClick(Sender: TObject);
begin
  if BTChest.Tag = CTagSearch then
  begin
    EDChestResult.Text := '...';
    LChestThread := TChestThread.Create(TRUE);
    LChestThread.Priority := tpHigher;
    LChestThread.FreeOnTerminate := TRUE;
    LChestThread.Start;
    BTChest.Caption := BTStopChestCaption;
    BTChest.Tag := CTagStop;
  end else
  begin
    BTChest.Tag := CTagStopRequested;
    AbortProcess(CChest);
  end;
end;

procedure TForm1.SetCaptions;
begin
  Caption := 'Mater GUI';
  with BTClipboard do begin Caption := BTClipboardCaption;  Hint := BTClipboardHint; end;
  with BTNext      do begin Caption := BTNextCaption;       Hint := BTNextHint; end;
  with BTOpen      do begin Caption := '...';               Hint := BTOpenHint; end;
  with GBFile      do begin Caption := GBFileCaption;       Hint := ''; end;
  with GBCount     do begin Caption := GBCountCaption;      Hint := GBCountHint; end;
  with GBIndex     do begin Caption := GBIndexCaption;      Hint := GBIndexHint; end;
  with GBParams    do begin Caption := GBParamsCaption;     Hint := GBParamsHint; end;
  with CBCheck     do begin Caption := CBCheckCaption;      Hint := CBCheckHint; end;
  with GBMater     do begin Caption := GBMaterCaption;      Hint := ''; end;
  with GBChest     do begin Caption := GBChestCaption;      Hint := ''; end;
  with BTMater     do begin Caption := BTStartMaterCaption; Hint := ''; end;
  with BTChest     do begin Caption := BTStartChestCaption; Hint := ''; end;
  with BTQuit      do begin Caption := BTQuitCaption;       Hint := ''; end;
  with GBFen       do begin Caption := GBFenCaption;        Hint := GBFenHint; end;
  with MIApp       do begin Caption := MIAppCaption;        Hint := ''; end;
  with MIQuit      do begin Caption := BTQuit.Caption;      Hint := ''; end;
  with MIHelp      do begin Caption := MIHelpCaption;       Hint := ''; end;
  with MIAbout     do begin Caption := MIAboutCaption;      Hint := ''; end;
end;

procedure TForm1.AbortProcess(const AExeName: string);
var
  LStr: string;
begin
  DebugLn(Format('AbortProcess(%s)', [AExeName]));
{$IFDEF MSWINDOWS}
  DebugLn(Format('RunCommand=%d', [Ord(RunCommand('taskkill', ['/f', '/im', AExeName], LStr))]));
{$ELSE}
{$IFDEF LINUX}
  DebugLn(Format('RunCommand=%d', [Ord(RunCommand('killall', [AExeName], LStr))]));
{$ELSE}
{$ENDIF}
{$ENDIF}
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  LSquares: TBGRABitmap;
  LCoordinates: TBGRABitmap;
  LPosition: string;
  LDepth: integer;
  i: integer;
begin
  LogLn('Free Pascal ' + {$I %FPCVERSION%} + ' ' + {$I %DATE%} + ' ' + {$I %TIME%} + ' ' + {$I %FPCTARGETOS%} + '-' + {$I %FPCTARGETCPU%}, TRUE);

  SetCaptions;
  LSquares := TBGRABitmap.Create(CSquaresBitmap);
  LCoordinates := TBGRABitmap.Create(CCoordinatesBitmap);
  LChessboard := TBGRABitmap.Create(400, 400, BGRAWhite);
  LChessboard.PutImage(40, 40, LSquares, dmSet);
  LChessboard.PutImage(0, 0, LCoordinates, dmDrawWithTransparency);
  LSquares.Free;
  LCoordinates.Free;

  for i := 0 to 31 do
  begin
    LPieces[i] := TPiece.Create(Panel1, BGRABitmapCollection, i);
    with LPieces[i] do
    begin
      Parent := Panel1;
      Left := -40;
      Top := 0;
    end;
  end;

  Screen.Cursors[GRAB] := LoadCursorFromLazarusResource('grab');
  Screen.Cursors[GRABBING] := LoadCursorFromLazarusResource('grabbing');
  LFilter := TFenFilter.Create;
  LoadSettings(LPosition, FFile, FPositionIndex, LDepth);
  EDDepth.Value := LDepth;
  FPositionList := TFenStringList.Create;
  LoadPositions;

  if (ParamCount = 1) and LFilter.IsFen(ParamStr(1)) then
    SetPosition(ParamStr(1))
  else
    if (ParamCount = 1) and FileExists(ParamStr(1)) then
    begin
      FFile := ParamStr(1);
      FPositionIndex := 1;
      LoadPositions;
      SetPositionFromFile(FPositionIndex);
    end else
      SetPosition(LPosition);
end;

procedure TForm1.FormDestroy(Sender: TObject);
var
  i: integer;
begin
  SaveSettings(FPosition, FFile, FPositionIndex, EDDepth.Value);
  for i := 0 to 31 do
    LPieces[i].Free;
  LChessboard.Free;
  LFilter.Free;
  FPositionList.Free;
end;

procedure TForm1.MIAboutClick(Sender: TObject);
begin
  FormAbout.Show;
end;

procedure TForm1.Panel1Paint(Sender: TObject);
begin
  LChessboard.Draw(Panel1.Canvas, 4, 4);
end;

procedure TForm1.SetPosition(const AEpd: string);
var
  LUsed: array[0..31] of boolean;

  procedure PlacePiece(const APieceType: string; const x, y: integer);
  var
    i: integer;
  begin
    i := 0;
    while (i <= 31) and (LUsed[i] or (APieceType <> LPieces[i].PieceType)) do
      Inc(i);
    if i <= 31 then
    begin
      with LPieces[i] do
      begin
        Left := 40 * x + 4;
        Top := 40 * (9 - y) + 4;
      end;
      LUsed[i] := TRUE;
    end;
  end;

var
  s: string;
  c: char;
  i, x, y: integer;
begin
  FPosition := AEpd;
  s := ExtractWord(1, FPosition, [' ']);
  for i := 0 to 31 do LUsed[i] := FALSE;
  i := 1;
  y := 8;
  x := 1;
  while i <= Length(s) do
  begin
    c := s[i];
    case c of
      '1'..'8': while c > '0' do begin Inc(x); Dec(c); end;
      '/': begin x := 1; Dec(y); end;
      'P': begin PlacePiece('wp', x, y); Inc(x); end;
      'R': begin PlacePiece('wr', x, y); Inc(x); end;
      'N': begin PlacePiece('wn', x, y); Inc(x); end;
      'B': begin PlacePiece('wb', x, y); Inc(x); end;
      'Q': begin PlacePiece('wq', x, y); Inc(x); end;
      'K': begin PlacePiece('wk', x, y); Inc(x); end;
      'p': begin PlacePiece('bp', x, y); Inc(x); end;
      'r': begin PlacePiece('br', x, y); Inc(x); end;
      'n': begin PlacePiece('bn', x, y); Inc(x); end;
      'b': begin PlacePiece('bb', x, y); Inc(x); end;
      'q': begin PlacePiece('bq', x, y); Inc(x); end;
      'k': begin PlacePiece('bk', x, y); Inc(x); end;
    else
      begin
        ShowMessage(Format('Invalid string: %s', [FPosition]));
        Exit;
      end;
    end;
    Inc(i);
  end;

  for i := 0 to 31 do
    if not LUsed[i] then
      with LPieces[i] do
      begin
        Left := -40;
        Top := 0;
      end;

  EDFen.Text := FPosition;
  EDMaterResult.Clear;
  EDChestResult.Clear;
end;

procedure TForm1.SetPositionFromFile(var APositionIndex: integer);
var
  LStr: string;
begin
  LogLn(Format('SetPositionFromFile(%d)', [APositionIndex]));
  EDIndex.Text := IntToStr(APositionIndex);
  if (FPositionList.Count > 0) and (APositionIndex <= FPositionList.Count) then
  begin
    LStr := FPositionList[Pred(APositionIndex)];
    SetPosition(LStr);
    if APositionIndex = FPositionList.Count then
      APositionIndex := 1
    else
      Inc(APositionIndex);
  end;
end;

procedure TForm1.LoadPositions;
begin
  if FileExists(FFile) then
  begin
    FPositionList.LoadFromFile(FFile);
    EDFile.Text := ExtractFileName(FFile);
    EDCount.Text := Format('%d', [FPositionList.Count]);
  end else
    ShowMessage(Format('Cannot find "%s"', [FFile]));
end;

initialization
{$I cursors.lrs}

end.

