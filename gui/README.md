# Mater GUI

## Overview

*Mater GUI* is a graphical user interface for *Mater*, a mate solving program written in Pascal by Valentin Albillo.

![Mater GUI for Linux Mageia](images/screenshot.png)

### Chest

*Mater GUI* uses Chest by Heiner Marxen as auxiliary search engine.

The Linux version of *Mater GUI* uses [Chest 3.19](http://turbotm.de/~heiner/Chess/chest.html). The Windows version uses [WinChest](https://fhub.jimdofree.com) by Heiner Marxen and Franz Huber.

## Usage

The position can be loaded either from the clipboard, from a file or from the command line.

### The art of mate in two

*Mater GUI* comes with its own FEN file. It is a collection of mates in two moves by Eduardo Sadier ([El Arte del Mate en Dos](https://sites.google.com/site/edusadier/home)).

This is the file selected by default. You can choose another file (any file containing EDP/FEN strings).

### Command line parameters

You can run the application with a position in EPD/FEN format or with the name of a file as parameter.

    matergui "b7/PP6/8/8/7K/6B1/6N1/4R1bk w - -"
    matergui "my-problems-collection.fen" 

## Compilation

*Mater GUI* is a [Lazarus](https://www.lazarus-ide.org/index.php) project.
