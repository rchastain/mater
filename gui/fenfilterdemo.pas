
{$assertions on}

uses
  SysUtils, FenFilter;

var
  LFilter: TFenFilter;
  
begin
  LFilter := TFenFilter.Create;

  Assert(LFilter.IsFen('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -'));
  Assert(LFilter.IsFen('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'));
  Assert(not LFilter.IsFen('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 bla'));
  Assert(not LFilter.IsFen('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR/ w KQkq -'));
  
  LFilter.Free;
end.
