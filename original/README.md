# Mater

In this folder you can find 1° the [file](mater.txt) that I downloaded some years ago on Valentin Albillo's website; 2° the [same file](mater.pas) with minimal modifications.

## Usage

The program expects four parameters:

  * Piece placement
  * Side to move
  * Moves number
  * Search mode (a=all moves, c=checks)

Command examples:

    mater 4b1N1/2pr2pR/2rp1p2/1Q1B1kp1/2pP4/4P1K1/1n3PN1/R7 w 4 a
    mater 3nn3/2p2p1k/1p1pp1p1/p2B3p/r2B2N1/7N/8/7K w 12 c

The search mode can be omitted. The default mode is "all moves".

## Modifications

  * Free Pascal compatibility
  * Two fixes in pawn moves generation (thanks to F. Huber)
  * Extra "/" no longer needed at the end of the first parameter
  * Fourth parameter can be omitted
