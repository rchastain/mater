ifeq ($(OS),Windows_NT)
	RM := del /q
else
	RM := rm -f
endif

.PHONY: mater demo clean
mater:
	@fpc @extrafpc.cfg mater.pas -dRELEASE
debug:
	@fpc @extrafpc.cfg mater.pas -dDEBUG
demo:
	@fpc @extrafpc.cfg demo.pas -dRELEASE
clean:
ifeq ($(OS),Windows_NT)
	@$(RM) units\*.o
	@$(RM) units\*.ppu
else
	@$(RM) units/*.o
	@$(RM) units/*.ppu
endif
