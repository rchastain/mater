
(*******************************************************************)
(*                                                                 *)
(*  MATER: Mate searching program - (c) Valentin Albillo 1998      *)
(*                                                                 *)
(*      This program or parts thereof can be used for any purpose  *)
(*  whatsoever as long as proper credit is given to the copyright  *)
(*  holder. Absolutely no guarantees given, no liabilities of any  *)
(*  kind accepted. Use at your own risk.  Your using this code in  *)
(*  all or in part does indicate your acceptance of these terms.   *)
(*                                                                 *)
(*******************************************************************)

program Mater;

uses
  SysUtils, Classes, MaterCore, CommandLine, FenStringList;

const
  CProgramName = 'Mate searching program v1.1 (c) Valentin Albillo 1998 r' + {$I version};
  CUsage = {$I usage};

procedure Solve(const AEpd: string; const ADepth: integer; const AMode: TSearchMode);
var
  LResult: string;
  LResultDepth: integer;
begin
  LResult := SolveMate(AEpd, ADepth, AMode, LResultDepth);
  
  if Length(LResult) = 0 then
    WriteLn('No mate found')
  else
    WriteLn(Format('Found mate in %d: %s', [LResultDepth, LResult]));
end;

procedure SolveAll(const AFile: string; const ADepth: integer);
var
  LLst: TStringList;
  LIdx, LCnt: integer;
  LTime: cardinal;
begin
  LLst := TFenStringList.Create;
  LLst.LoadFromFile(AFile);
  WriteLn('Found ', LLst.Count, ' positions');
  WriteLn('Searching for solutions...');
  
  LCnt := 0;
  LTime := GetTickCount64;
  for LIdx := 0 to Pred(LLst.Count) do
    if Length(SolveMate(LLst[LIdx], ADepth, smAllMoves)) > 0 then
      Inc(LCnt)
    else
      WriteLn('Cannot solve: ', Succ(LIdx), '=', LLst[LIdx]);
  
  LTime := GetTickCount64 - LTime;
  WriteLn('Time elapsed ', FormatDateTime('hh:nn:ss:zzz', LTime / (1000 * SECSPERDAY)));
  WriteLn('Solved ', LCnt, '/', LLst.Count);
  LLst.Free;
end;

var
  LEpd: string;
  LDepth: integer;
  LMode: TSearchMode;
  LFile: string;
  LNeedHelp: boolean;
  
begin
  LEpd := '';
  LDepth := 0;
  LMode := smAllMoves;
  LFile := '';
  LNeedHelp := FALSE;
  LVerbose := not HasOption('q', 'quiet');
  if HasOption('p', 'position') then
    LEpd := GetOptionValue('p', 'position')
  else if HasOption('f', 'file') then
  begin
    LFile := GetOptionValue('f', 'file');
    LVerbose := FALSE;
    LLog := FALSE;
  end else
    LNeedHelp := TRUE;
  if LVerbose then
    WriteLn(CProgramName);
  if HasOption('m', 'moves') then
    LDepth := StrToIntDef(GetOptionValue('m', 'moves'), 0)
  else
    LNeedHelp := TRUE;
  if HasOption('c', 'check') then
    LMode := smChecks;
  if HasOption('n', 'nolog') then
    LLog := FALSE;
  if LNeedHelp then
    WriteLn(CUsage)
  else if Length(LFile) = 0 then
    Solve(LEpd, LDepth, LMode)
  else if FileExists(LFile) then
    SolveAll(LFile, LDepth)
  else
    WriteLn('File not found: [', LFile, ']');
end.
