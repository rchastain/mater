
unit MaterUtils;

interface

uses
  SysUtils, StrUtils, MaterTypes;

function PieceColor(const ABoard: integer): integer;
function IsDarkSquare(const ASquare: integer): boolean;
function SquareToStr(const ASquare: integer): string;
function StrToSquare(const AStr: string): integer;
function FormatMove(const AMove: TMove): string;
function CastlingStateToStr(const AKingCastle, AKingRookCastle, AQueenRookCastle: TBooleanArray): string;
function PosToStr(const APos: TPosition): string;
function LoadPosition(const AFen: string; var APosition: TPosition; var AActiveColor: integer): boolean;
procedure LogLn(const ALine: string; const ARewrite: boolean = FALSE);

implementation

function PieceColor(const ABoard: integer): integer;
begin
  if ABoard = CBlank then
    result := CNone
  else if ABoard < CBlank then
    result := CBlack
  else
    result := CWhite;
end;

function IsDarkSquare(const ASquare: integer): boolean;
begin
  result := ((ASquare mod 10) + (ASquare div 10)) mod 2 = 1;
end;

function SquareToStr(const ASquare: integer): string;
var
  x, y: integer;
begin
  x := ASquare mod 10 - 2;
  y := 9 - ASquare div 10;
  if (x >= 0) and (x <= 7) and (y >= 0) and (y <= 7) then 
    result := Concat(
      Chr(x + Ord('a')),
      Chr(y + Ord('1'))
    )
  else
    result := '--';
end;

function StrToSquare(const AStr: string): integer;
begin
  if Length(AStr) = 2 then
    //result := 10 * (Ord(AStr[1]) - Ord('a') + 2) + Ord(AStr[2]) - Ord('1') + 2
    result := 10 * (9 - Ord(AStr[2]) + Ord('1')) + Ord(AStr[1]) - Ord('a') + 2 // 20/11/2021
  else
    result := 0;
end;

function FormatMove(const AMove: TMove): string;

  function ClassToStr(const AClass: integer): string;
  begin
    case Abs(AClass) of
      CAny:         result := '';
      CKnight:      result := 'n';
      CBishop:      result := 'b';
      CRook:        result := 'r';
      CQueen:       result := 'q';
      CShortCastle: result := '';
      CLongCastle:  result := '';
      CEnPassant:   result := '';
      else
        result := '?';
    end;
  end;

begin
  with AMove do
    if (FFrom = 0) and (FTo = 0) then
      result := '(no move)'
    else
      result := Concat(SquareToStr(FFrom), SquareToStr(FTo), ClassToStr(FClass));
end;

function CastlingStateToStr(const AKingCastle, AKingRookCastle, AQueenRookCastle: TBooleanArray): string;
begin
  result := Format('%d%d%d%d%d%d', [
    Ord(AKingCastle[CWhite]),
    Ord(AKingRookCastle[CWhite]),
    Ord(AQueenRookCastle[CWhite]),
    Ord(AKingCastle[CBlack]),
    Ord(AKingRookCastle[CBlack]),
    Ord(AQueenRookCastle[CBlack])
  ]);
end;

function PosToStr(const APos: TPosition): string;
const
  CArrow: array[boolean] of string = ('', '<<');
  CBoard =
    '' + LineEnding +
    '      A   B   C   D   E   F   G   H  ' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  8 | %s | %s | %s | %s | %s | %s | %s | %s | %s' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  7 | %s | %s | %s | %s | %s | %s | %s | %s |' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  6 | %s | %s | %s | %s | %s | %s | %s | %s |' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  5 | %s | %s | %s | %s | %s | %s | %s | %s |' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  4 | %s | %s | %s | %s | %s | %s | %s | %s |' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  3 | %s | %s | %s | %s | %s | %s | %s | %s |' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  2 | %s | %s | %s | %s | %s | %s | %s | %s |' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '  1 | %s | %s | %s | %s | %s | %s | %s | %s | %s' + LineEnding +
    '    +---+---+---+---+---+---+---+---+' + LineEnding +
    '' + LineEnding +
    '    Side to move: %s' + LineEnding +
    '    Castling    : %s' + LineEnding +
    '    En passant  : %s' + LineEnding;
var
  LCastling: string;
  LEnPassant: string;
begin
  with APos do
  begin
    LCastling := IfThen(
      FKingRookCastle[CWhite] or FQueenRookCastle[CWhite] or FKingRookCastle[CBlack] or FQueenRookCastle[CBlack], 
      Concat(
        IfThen(FKingRookCastle[CWhite],  'K', ''),
        IfThen(FQueenRookCastle[CWhite], 'Q', ''),
        IfThen(FKingRookCastle[CBlack],  'k', ''),
        IfThen(FQueenRookCastle[CBlack], 'q', '')
      ),
      '-'
    );
    LEnPassant := IfThen(FEnPassantSquare = CNone, '-', SquareToStr(FEnPassantSquare));
    result := Format(
      CBoard,
      [
        CShape[FBoard[22]], CShape[FBoard[23]], CShape[FBoard[24]], CShape[FBoard[25]], CShape[FBoard[26]], CShape[FBoard[27]], CShape[FBoard[28]], CShape[FBoard[29]], CArrow[FActiveColor],
        CShape[FBoard[32]], CShape[FBoard[33]], CShape[FBoard[34]], CShape[FBoard[35]], CShape[FBoard[36]], CShape[FBoard[37]], CShape[FBoard[38]], CShape[FBoard[39]], 
        CShape[FBoard[42]], CShape[FBoard[43]], CShape[FBoard[44]], CShape[FBoard[45]], CShape[FBoard[46]], CShape[FBoard[47]], CShape[FBoard[48]], CShape[FBoard[49]], 
        CShape[FBoard[52]], CShape[FBoard[53]], CShape[FBoard[54]], CShape[FBoard[55]], CShape[FBoard[56]], CShape[FBoard[57]], CShape[FBoard[58]], CShape[FBoard[59]], 
        CShape[FBoard[62]], CShape[FBoard[63]], CShape[FBoard[64]], CShape[FBoard[65]], CShape[FBoard[66]], CShape[FBoard[67]], CShape[FBoard[68]], CShape[FBoard[69]], 
        CShape[FBoard[72]], CShape[FBoard[73]], CShape[FBoard[74]], CShape[FBoard[75]], CShape[FBoard[76]], CShape[FBoard[77]], CShape[FBoard[78]], CShape[FBoard[79]], 
        CShape[FBoard[82]], CShape[FBoard[83]], CShape[FBoard[84]], CShape[FBoard[85]], CShape[FBoard[86]], CShape[FBoard[87]], CShape[FBoard[88]], CShape[FBoard[89]], 
        CShape[FBoard[92]], CShape[FBoard[93]], CShape[FBoard[94]], CShape[FBoard[95]], CShape[FBoard[96]], CShape[FBoard[97]], CShape[FBoard[98]], CShape[FBoard[99]], CArrow[FActiveColor = FALSE],
        CColor[FActiveColor],
        LCastling,
        LEnPassant
      ]
    );
  end;
end;

function LoadPosition(const AFen: string; var APosition: TPosition; var AActiveColor: integer): boolean;
var
  a: array[1..6] of string;
  x, y, i, j: integer;
begin
  result := FALSE;
  
  for i := 1 to 6 do
    a[i] := ExtractWord(i, AFen, [' ']);

  x := 1;
  y := 8;
  i := 1;

  with APosition do
  begin
    while i <= Length(a[1]) do
    begin
      case UpCase(a[1][i]) of
        'P', 'N', 'B', 'R', 'Q', 'K':
          begin
            case a[1][i] of
              'p': j := CBlack * CPawn;
              'n': j := CBlack * CKnight;
              'b': j := CBlack * CBishop;
              'r': j := CBlack * CRook;
              'q': j := CBlack * CQueen;
              'k': j := CBlack * CKing;
              'P': j := CWhite * CPawn;
              'N': j := CWhite * CKnight;
              'B': j := CWhite * CBishop;
              'R': j := CWhite * CRook;
              'Q': j := CWhite * CQueen;
              'K': j := CWhite * CKing;
            end;
            FBoard[10 * (10 - y) + x + 1] := j;
            Inc(x);
          end;
        '1'..'8':
          begin
            j := Ord(a[1][i]) - Ord('0');
            while j > 0 do
            begin
              FBoard[10 * (10 - y) + x + 1] := cBlank;
              Inc(x);
              Dec(j);
            end;
          end;
        '/':
          begin
            x := 1;
            Dec(y);
          end;
      else
        Exit;
      end;
      Inc(i);
    end;
    
    FActiveColor := a[2] = 'b';
     
    FQueenRookCastle[CBlack] := (Pos('q', a[3]) > 0);
    FKingRookCastle[CBlack] := (Pos('k', a[3]) > 0);
    FKingCastle[CBlack] := FQueenRookCastle[CBlack] or FKingRookCastle[CBlack];

    FQueenRookCastle[CWhite] := (Pos('Q', a[3]) > 0);
    FKingRookCastle[CWhite] := (Pos('K', a[3]) > 0);
    FKingCastle[CWhite] := FQueenRookCastle[CWhite] or FKingRookCastle[CWhite];

    if a[4] = '-' then
      FEnPassantSquare := CNone
    else
      FEnPassantSquare := StrToSquare(a[4]);
  end;

  if a[2] = 'w' then
    AActiveColor := CWhite
  else if a[2] = 'b' then
    AActiveColor := CBlack
  else
    Exit;

  result := TRUE;
end;

var
  LFilename: TFilename;
  
procedure LogLn(const ALine: string; const ARewrite: boolean);
var
  LFile: TextFile;
begin
  Assign(LFile, LFilename);
  if ARewrite or not FileExists(LFileName) then
    Rewrite(LFile)
  else
    Append(LFile);
  WriteLn(LFile, ALine);
  Close(LFile);
end;

initialization
  LFilename := ChangeFileExt(ParamStr(0), '.log');
finalization
  LFilename := '';
end.
