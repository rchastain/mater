  'Usage:' + LineEnding +
  '  mater -p position -m movesnumber [-c]' + LineEnding +
  '  mater -f file -m movesnumber [-c]' + LineEnding +
  'Options:' + LineEnding +
  '  -p, -position  The position to solve (an EPD/FEN string)' + LineEnding +
  '  -m, -moves     The moves number' + LineEnding +
  '  -c, -checks    Mate by check sequences' + LineEnding +
  '  -f, -file      Solve all positions of the file' + LineEnding +
  '  -q, -quiet     Minimal output' + LineEnding +
  '  -n, -nolog     Do not create mater.log' + LineEnding +
  'Example:' + LineEnding +
  '  mater -p "b7/PP6/8/8/7K/6B1/6N1/4R1bk w - -" -m 3' + LineEnding +
  '  mater -p "3nn3/2p2p1k/1p1pp1p1/p2B3p/r2B2N1/7N/8/7K w KQkq -" -m 12 -c' + LineEnding +
  '  mater -f mate-in-2.epd -m 2'
